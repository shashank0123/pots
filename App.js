import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";
import Navigation from "./screens/Navigation";
class App extends Component {
  render() {
    return (
      <Navigation />
    );
  }
}
export default App;
