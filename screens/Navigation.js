import React, { Component } from 'react';
import { Animated, Easing, Platform ,StyleSheet,AsyncStorage,Image, Text, View} from 'react-native';
// import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {BottomTabBar,createStackNavigator,createSwitchNavigator,createBottomTabNavigator,createDrawerNavigator, createAppContainer, DrawerItems } from "react-navigation";
import Icon from 'react-native-vector-icons/Ionicons';


import Login from '../screens/Login';
import Register from '../screens/Register';
import Dashboard from '../screens/Dashboard';
import Subscribe from '../screens/Subscribe';
import Account from '../screens/Account';
import MyAddress from '../screens/MyAddress';
import Filters from '../screens/Filters';
import Filter2 from '../screens/Filter2';
import ProductDetails from '../screens/ProductDetails';
import ProductDetails2 from '../screens/ProductDetails2';
import PlanStart from '../screens/PlanStart';
import Search from '../screens/Search';
import MyOrders from '../screens/MyOrders';
import Checkout from '../screens/Checkout';
import Payments from '../screens/Payments';
import OrderDetails from '../screens/OrderDetails';




// import { createStackNavigator, createAppContainer } from 'react-navigation';




let SlideFromRight = (index, position, width) => {
  const translateX = position.interpolate({
    inputRange: [index - 1, index],
    outputRange: [width, 0],
  })

  return { transform: [ { translateX } ] }
};

let SlideFromBottom = (index, position, height) => {
  const translateY = position.interpolate({
    inputRange: [index - 1, index],
    outputRange: [height, 0],
  })

  return { transform: [ { translateY } ] }
};

let CollapseTransition = (index, position) => {
  const opacity = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0, 1, 1]
  });

  const scaleY = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0, 1, 1]
  });

  return {
    opacity,
    transform: [ { scaleY } ]
  }
}

const TransitionConfiguration = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: (sceneProps) => {
      const { layout, position, scene } = sceneProps;
      const width = layout.initWidth;
      const height = layout.initHeight;
      const { index, route } = scene
      const params = route.params || {}; // <- That's new
      const transition = params.transition || 'default'; // <- That's new
      return {
        rightTransition: SlideFromRight(index, position, width),
        default: SlideFromBottom(index, position, height),
        bottomTransition: SlideFromBottom(index, position, height),
        collapseTransition: CollapseTransition(index, position)
      }[transition];
    },
  }
}

const TabBarComponent = (props) => (<BottomTabBar {...props} />)

const BAppNav=createBottomTabNavigator({
  Try: {screen: Dashboard,navigationOptions:{
    tabBarLabel:'Try',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('../assets/try.png')}  style={{width:35, height:35}}  />
            : <Image source={require('../assets/try1.png')}  style={{width:35, height:35, marginTop:30,}}  /> 
    ) 
  }},
  Subscribe: {screen: Subscribe,navigationOptions:{  
    tabBarLabel:'Subscribe',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('../assets/subscribe.png')}  style={{width:35, height:35}}  />
            : <Image source={require('../assets/subscribe1.png')}  style={{width:35, height:35, marginTop:30,}}  /> 
    ) 
  }},
  Account: {screen: Account,navigationOptions:{  
    tabBarLabel:'Account',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('../assets/account.png')}  style={{width:35, height:35}}  />
            : <Image source={require('../assets/account1.png')}  style={{width:35, height:35, marginTop:30,}}  /> 
    ) 
  }},
  // Filter2: {screen: Filter2,navigationOptions:{  
  //   tabBarLabel:'Filter',  
  //   tabBarIcon: ({focused}) =>(
  //     focused
  //           ? <Image source={require('./assets/filter.png')}  style={{width:35, height:35}}  />
  //           : <Image source={require('./assets/filter1.png')}  style={{width:35, height:35}}  /> 
  //   ) 
  // }},
},{
  tabBarComponent: props => {
		return (
			<View style={{
				position: 'absolute',
				left: 0,
				right: 0,
        bottom: 0,
        
        
			}}>
        <TabBarComponent {...props} />
			</View>
		)
	},
  tabBarOptions: {
    activeTintColor: "#A0A0A0",      
    inactiveTintColor: "#ffffff",  
    style: {
      paddingHorizontal:80,
      height:100,
      paddingVertical:2, 
      borderTopRightRadius:30, 
      borderTopLeftRadius:30, 
      // borderColor:'red',
      // borderWidth:1,
      backgroundColor:'#fff'
    },
    // activeTabStyle: {
    //   marginBottom:8,
    // },
    labelStyle: {
      fontSize: 10,
      margin: 0,
      marginTop:-15,
      padding: 10,
      fontWeight:'bold',
      
    },
  }
  });

const RootStack = createStackNavigator({
 Login: { screen: Login,navigationOptions: {header: null} },
//  Dashboard: { screen: Dashboard},
Dashboard:{screen:BAppNav,navigationOptions: {header: null,}}, //Check This.
 Subscribe: { screen: Subscribe,navigationOptions: {header: null} },
 Account: { screen: Account,navigationOptions: {header: null} },
 Register:{screen:Register,navigationOptions: {header: null}},
  MyAddress:{screen:MyAddress,navigationOptions: {header: null}},
  ProductDetails2:{screen:ProductDetails2,navigationOptions: {header: null}},
  ProductDetails:{screen:ProductDetails,navigationOptions: {header: null}},
  Filter2:{screen:Filter2,navigationOptions: {header: null}},
  OrderDetails:{screen:OrderDetails,navigationOptions: {header: null}},
  Payments:{screen:Payments,navigationOptions: {header: null}},
  Checkout:{screen:Checkout,navigationOptions: {header: null}},
  MyOrders:{screen:MyOrders,navigationOptions: {header: null}},
  Search:{screen:Search,navigationOptions: {header: null}},
  PlanStart:{screen:PlanStart,navigationOptions: {header: null}},
  
 
}, 
{
    initialRouteName: 'Login',
    // headerMode: 'screen',
    transitionConfig: TransitionConfiguration,
});

// export default Navigation
const AppContainer = createAppContainer(RootStack);
  
  export default class Navigation extends Component {
    render() {
      return <AppContainer />;
    }
  }