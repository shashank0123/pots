import React from 'react';
import {
  StatusBar, StyleSheet, KeyboardAvoidingView, AsyncStorage, ImageBackground, Dimensions, FlatList, ScrollView,
  Text, Image, View, TextInput, TouchableOpacity, ActivityIndicator, BackHandler
} from 'react-native';
import * as Animatable from 'react-native-animatable';

import { Container, Content, Header, Left, Card, CardItem, Tab, Tabs, Body, Icon, Right, Title } from 'native-base';
import { BoxShadow } from 'react-native-shadow'
const height = Math.round(Dimensions.get('window').height);
const width = Math.round(Dimensions.get('window').width);
const screenWidth = width * 48 / 100;
const sc3 = width * 60 / 100;
const sc4 = width * 70 / 100;
const dropShadow = {
  width: width * 0.8,
  height: 80,
  color: "#ffffff",
  border: 1,
  radius: 40,
  overflow: 'hidden',
  opacity: 1,
  x: 10,
  y: 49,
  style: {
    marginRight: width * 0.05,
    // marginTop:height*0.1, 
    borderBottomRightRadius: width * 0.1,
    borderBottomLeftRadius: width * 0.1,
    alignSelf: 'center',
  }
}
const dropShadow1 = {
  width: width * 0.15,
  height: 25,
  color: "#000000",
  border: 1,
  radius: 12.5,
  opacity: 0.2,
  x: 10,
  y: -12,
  style: { marginRight: width * 0.05, marginTop: -height * 0.01, borderBottomRightRadius: width * 0.1, borderBottomLeftRadius: width * 0.1, alignSelf: 'center', }
}
export default class Payments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewAnimation: "fadeInRightBig"
    }
    
  }
  async componentDidMount() {
    this.setState({ assetsLoaded: true });
    
  }
 
  render() {
    const { assetsLoaded } = this.state;
    if (assetsLoaded) {
      return (
        <ScrollView style={{ flex: 1, backgroundColor: '#eaeaea' }}>
       
            <ImageBackground source={require('.././assets/Payments_BG.png')} resizeMode="cover" style={{ width: '100%', height: height * 1.07 }}>
              <View style={{ flex: 1, padding: 40 }}>
                <TouchableOpacity
                  //  onPress={() => this.setState({ viewAnimation: 'fadeOutRightBig', screenExit: true })}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Text style={{ fontSize: 12, fontFamily: 'Eina03-Bold', color: '#fff' }}>back</Text>
                  <Image style={{ width: '25%', height: 12, marginVertical: 5, borderRadius: 20, marginLeft: -40 }}
                    source={require(".././assets/Wave_line.png")} />
                </TouchableOpacity>
                <Text style={{ fontSize: 26, fontFamily: 'Eina03-Bold', color: 'black' }}>Payments</Text>
              </View>
              <View style={{ flex: 3, paddingHorizontal: 40, marginTop: height * 0.08 }}>
                <Text style={{ fontSize: 20, fontFamily: 'Eina03-Bold', color: 'black', marginBottom: 10 }}>Cards</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Image style={{ width: 130, height: 80, borderRadius: 10 }} source={require(".././assets/card1.jpg")} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Image style={{ width: 130, height: 80, borderRadius: 10 }} source={require(".././assets/card2.jpg")} />
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Image style={{ width: 130, height: 80, borderRadius: 10 }} source={require(".././assets/newcard.jpg")} />
                  </TouchableOpacity>
                </View>
                <Text style={{ fontSize: 20, fontFamily: 'Eina03-Bold', color: 'black', marginBottom: 10, marginTop: 20 }}>Wallets</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Card style={{ borderRadius: 10, padding: 0, }}>
                      <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={require(".././assets/paytm.jpg")} />
                    </Card>
                    {/* <BoxShadow setting={dropShadow1}>
                    </BoxShadow> */}
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Card style={{ borderRadius: 10, padding: 0, }}>
                      <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={require(".././assets/paypal.jpg")} />
                    </Card>
                    {/* <BoxShadow setting={dropShadow1}>
                    </BoxShadow> */}
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Card style={{ borderRadius: 10, padding: 0, }}>
                      <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={require(".././assets/gpay.jpg")} />
                    </Card>
                    {/* <BoxShadow setting={dropShadow1}>
                    </BoxShadow> */}
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails')}>
                    <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={require(".././assets/newq.jpg")} /></TouchableOpacity>
                </View>
              </View>
              {/* <BoxShadow setting={dropShadow}>
              </BoxShadow> */}
            </ImageBackground>
         
        </ScrollView>
      );
    } else {
      return (
        <View>
          <ActivityIndicator />
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  textWithShadow: {
    textShadowColor: '#fff', fontFamily: 'custom-fonts',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10, fontSize: 24, color: '#fff'
  }, btn: { width: '45%', padding: 10, backgroundColor: '#fff', borderRadius: 20 },
  btn2: { width: '45%', padding: 10, backgroundColor: '#F2F3F4', borderRadius: 20 },
  btnText: { fontSize: 18, textAlign: 'center', color: '#34495E', fontFamily: 'custom-fonts' }
});